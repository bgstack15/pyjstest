#!/usr/bin/env python3
# Purpose: Display the name of the attached game controller
from sdl2 import *
import sdl2.ext, ctypes

def get_joystick():
   running = True
   SDL_Init(SDL_INIT_JOYSTICK)
   sdl2.ext.init()
   while running:
      event = SDL_Event()
      while SDL_PollEvent(ctypes.byref(event)) != 0:
         if event.type == SDL_JOYDEVICEADDED:
            b = SDL_JoystickOpen(event.jdevice.which)
            c = SDL_JoystickName(b).decode('UTF-8')
            print(f"Device: {c}")
            #running = False
            #return b

if __name__ == "__main__":
   get_joystick()
