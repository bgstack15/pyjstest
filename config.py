# File: config.py
# Package: pyjstest
# Usage: Add new colors, and gamepad configs here. See documented examples.

import sdl2.ext
colors = {
   "BKGD": sdl2.ext.Color(220, 220, 220),
   "RED": sdl2.ext.Color(255, 0,     0),
   "YELLOW": sdl2.ext.Color(255, 255,   0),
   "GREEN": sdl2.ext.Color(0,   255,   0),
   "BLUE": sdl2.ext.Color(50,   50,   255),
   "GRAY": sdl2.ext.Color(180, 180, 180),
   "DARK_RED": sdl2.ext.Color(180, 0,     0),
   "DARK_YELLOW": sdl2.ext.Color(180, 180,   0),
   "DARK_GREEN": sdl2.ext.Color(0,   180,   0),
   "DARK_BLUE": sdl2.ext.Color(0,   0,   100),
   "DARK_GRAY": sdl2.ext.Color(100, 100, 100)
}

CONFIG = {}
CONFIG['SNES gamepad'] = {
   "aliases": ["2Axes 11Keys Game Pad","usb gamepad"],
   "window": (300, 180),
   "buttons": [
      # color_on, color_off, x, y, size, button_id
      ["BLUE", "DARK_BLUE", 220,80, 20, 0],
      ["RED", "DARK_RED", 240,100, 20, 1],
      ["YELLOW", "DARK_YELLOW", 220,120, 20, 2],
      ["GREEN", "DARK_GREEN", 200,100, 20, 3],
      ["GRAY", "DARK_GRAY", 220, 40, 20, 5], # R bumper
      ["GRAY", "DARK_GRAY", 60, 40, 20, 4], # L bumper
      ["GRAY", "DARK_GRAY", 120, 105, 20, 8], # Select
      ["GRAY", "DARK_GRAY", 160, 105, 20, 9] # Start
   ],
   "dpads": [
      # color_on, color_off, x,y, size, axis, direction
      ["GRAY", "DARK_GRAY", 60, 80, 20, 1, "negative"],
      ["GRAY", "DARK_GRAY", 60, 120, 20, 1, "positive"],
      ["GRAY", "DARK_GRAY", 40, 100, 20, 0, "negative"],
      ["GRAY", "DARK_GRAY", 80, 100, 20, 0, "positive"]
   ],
   "dpad_axes": {
      "min":     [-32768, -32768],
      "mid_min": [-500,     -500],
      "mid_max": [500,       500],
      "max":     [32767,   32767]
   }
}

# This is for the Gamestop Xbox style controller
CONFIG["Xbox controller"] = {
   "aliases": ["PDP Xbox 360 Controller","PDP Xbox 360 Controller"],
   "window": (420, 240),
   "buttons": [
      # color_on, color_off, x, y, size, button_id
      ["GREEN", "DARK_GREEN", 340,160, 20, 0],
      ["RED", "DARK_RED", 360,140, 20, 1],
      ["BLUE", "DARK_BLUE", 320,140, 20, 2],
      ["YELLOW", "DARK_YELLOW", 340,120, 20, 3],
      ["GRAY", "DARK_GRAY", 60, 80, 20, 4], # L bumper
      ["GRAY", "DARK_GRAY", 340, 80, 20, 5], # R bumper
      ["GRAY", "DARK_GRAY", 140, 60, 20, 6], # Select
      ["GRAY", "DARK_GRAY", 200, 75, 20, 8], # Xbox
      ["GRAY", "DARK_GRAY", 260, 60, 20, 7], # Start
      # Do not use hese here, because they are associated with specific analog sticks.
      #["GRAY", "DARK_GRAY", 60, 140, 20, 9], # left analog
      #["GRAY", "DARK_GRAY", 260, 160, 20, 10], # right analog
   ],
   # Use these if you want to use an analog stick as a mere dpad (up, down, left, right)
   "dpads": [
      # color_on, color_off, x,y, size, axis, direction
      #["GRAY", "DARK_GRAY", 60, 120, 20, 1, "negative"],
      #["GRAY", "DARK_GRAY", 60, 160, 20, 1, "positive"],
      #["GRAY", "DARK_GRAY", 40, 140, 20, 0, "negative"],
      #["GRAY", "DARK_GRAY", 80, 140, 20, 0, "positive"],
      #["GRAY", "DARK_GRAY", 260, 140, 20, 4, "negative"],
      #["GRAY", "DARK_GRAY", 260, 180, 20, 4, "positive"],
      #["GRAY", "DARK_GRAY", 240, 160, 20, 3, "negative"],
      #["GRAY", "DARK_GRAY", 280, 160, 20, 3, "positive"],
      #["GRAY", "DARK_GRAY", 60, 40, 20, 2, "positive"], # L trigger
      #["GRAY", "DARK_GRAY", 340, 40, 20, 5, "positive"] # R trigger
   ],
   "hats": [
      # color_on, color_off, x,y, size, hat_id, up, right, down, left
      ["GRAY", "DARK_GRAY", 140, 160, 20, 0, 0, 1, 2, 4, 8] # binary value of the positions, off, up, right, down, left
   ],
   # Central location of all axes and their ranges. This is how you would calibrate the input.
   # index number of these values, is the axis number.
   "dpad_axes": {
      "min":     [-32768, -32768, -32768, -32768, -32768, -32768],
      "mid_min": [-3000,   -3000, -22000, -10000, -10000, -22000],
      "mid_max": [3000,     3000, -21999,  10000,  10000, -21999],
      "max":     [32767,   32767,  32767,  32767,  32767,  32767]
   },
   "analogs": [
      # color_on, color_off, center X, center Y, indicator_size, size, axes list, orientation list
      # and list of BUTTONs:
      #    color_on, color_off, relx, rely, size, button_id
      ["RED","DARK_GRAY", 70, 150, 4, 100, [0,1],[1,1],[
            # color_on, color_off, relx, rely, size, button_id
            ["YELLOW", "DARK_YELLOW", 4,4, 4, 9]
         ]
      ],
      ["RED","DARK_GRAY", 270, 150, 4, 100, [3,4],[1,1],[
            # color_on, color_off, relx, rely, size, button_id
            ["BLUE", "DARK_BLUE", 0,-4, 4, 10]
         ]
      ]
   ],
   "triggers": [
      # color_on, color_off, background, x1,y1, x2,y2, size, axis, direction
      # direction is not implemented.
      ["RED", "DARK_BLUE", "BKGD", 60,40, 79,40, 3, 2, 1],
      ["YELLOW", "DARK_BLUE", "GRAY", 350,40, 350,59, 3, 5, 1],
   ]
}
