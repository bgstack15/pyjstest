<!--
.. File: pyjstest/README.md
.. Location: https://bgstack15.ddns.net/cgit/pyjstest/
.. Author: bgstack15
.. Startdate: 2022-07-03
.. Title: README for pyjstest
.. Purpose: Describe this project
.. History:
.. Usage:
.. Reference:
.. Improve:
.. Documentation:
.. Dependencies:
-->
# README for pyjstest
Pyjstest is a python3 utility that displays a simple, graphical representation of the inputs of your attached gamepad.
[![](.images/pyjstest.png)](.images/pyjstest.png)

## Dependencies
On Devuan-like systems use packages:

* python3-sdl2
* python3-pygame

## How to use
You may attach the USB gamepad before or after running this program. For best results, do not connect multiple gamepads at a time.

Add new configs to `config.py`, with a simple declarative syntax that is documented briefly with the two given examples. Feel free to share additional configs with this upstream. Gather aliases for attached gamepads with the helper files in directory `extras/`.

## Upstream
The upstream for the project is at <https://bgstack15.ddns.net/cgit/pyjstest/> or <https://gitlab.com/bgstack15/pyjstest/>.

## Reason for existence
I wanted to build a simple input-reaction program, and test my python skills, and learn some sdl2. SDL2 has python3 bindings available, but most of the documentation on the Internet is for the C library.

## Alternatives
For basic input display on cli and gtk displays, use these utilities.

* jstest
* jstest-gtk

## Improvements
I need to redesign this to handle multiple attached devices. Right now, it supports only one.

## Reference
