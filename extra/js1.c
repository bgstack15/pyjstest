// File: js1.c
// Startdate: 2022-06-29
// Purpose: Test my C skills, while trying to test SDL2 game controller stuff
// References:
// http://www.libsdl.org/release/SDL-1.2.15/docs/html/guideinput.html
// https://wiki.libsdl.org/SDL_GameControllerMapping
// https://stackoverflow.com/q/29589982
// https://generalarcade.com/gamepadtool/
// https://wiki.winehq.org/Useful_Registry_Keys
// https://wiki.archlinux.org/title/Gamepad
// https://stackoverflow.com/questions/29589982/does-sdl-on-linux-support-more-than-one-gamepad-joystick
// Dependencies:
//    libsdl2-dev
// Documentation:
// try SDL_GAMECONTROLLERCONFIG env var.

#include "SDL2/SDL.h"
SDL_Joystick *joy;
SDL_GameController *ctrl;
char *guid[33];
char *mapping;
const char *newmapping = "03000000bd12000015d0000010010000,Tomee SNES USB Controller,platform:Linux,a:b2,b:b1,x:b3,y:b0,back:b8,start:b9,leftshoulder:b4,rightshoulder:b5,dpup:-a1,dpdown:+a1,dpleft:-a0,dpright:+a0,";

int main () {
   if (SDL_Init( SDL_INIT_VIDEO | SDL_INIT_JOYSTICK ) < 0)
   {
      fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
      exit(1);
   }
   SDL_InitSubSystem(SDL_INIT_JOYSTICK);
   SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER);
   printf("%i joysticks were found.\n\n", SDL_NumJoysticks() );
   printf("The names of the joysticks are:\n");

   for( int i=0; i < SDL_NumJoysticks(); i++ ) 
   {
      joy = SDL_JoystickOpen(i);
		if (joy) {
			printf("Opened joystick #%d \"%s\"\n", i, SDL_JoystickNameForIndex(i));
			printf("Axes count: %d\n", SDL_JoystickNumAxes(joy));
			printf("Button count: %d\n", SDL_JoystickNumButtons(joy));
			printf("Ball count: %d\n", SDL_JoystickNumBalls(joy));
			SDL_JoystickGetGUIDString(SDL_JoystickGetGUID(joy),*guid,sizeof(guid));
			printf("GUID string: \"%s\"\n",guid);
			if (SDL_IsGameController(i)) {
				ctrl = SDL_GameControllerOpen(i);
				mapping = SDL_GameControllerMapping(ctrl);
				printf("Mapping: \"%s\"\n", mapping);
				SDL_free(mapping);
				printf("Trying new mapping.\n");
				int r = SDL_GameControllerAddMapping(newmapping);
				printf("Result: %d.\n", r);
			}
		}
   }
   return 0;
}
